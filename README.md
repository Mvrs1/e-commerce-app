<!-- Please update value in the {}  -->

<h1 align="center">Audiophile e-commerce site</h1>

<div align="center">
   Solution for a challenge from  <a href="http://frontendmentor.io" target="_blank">Frontendmentor.io</a>.
</div>

<br>

<!-- TABLE OF CONTENTS -->

## Table of Contents

- [Overview](#overview)
  - [Built With](#built-with)
- [The Challenge](#the-challenge)
- [How to use](#how-to-use)
- [Contact](#contact)
- [Acknowledgements](#acknowledgements)

<!-- OVERVIEW -->

## Overview
![screenschot1](https://user-images.githubusercontent.com/5723692/128581726-46c773c1-3e5f-4650-ac9d-c1795e2c1363.png)
![screenshot-redux](https://user-images.githubusercontent.com/5723692/128581753-ba640f2a-d728-44e8-9a7f-44ed4df1c6af.png)


### Built With

<!-- This section should list any major frameworks that you built your project using. Here are a few examples.-->

- [React](https://reactjs.org/)
- [Next.js](https://nextjs.org/)
- [Redux Toolkit](https://redux-toolkit.js.org/)
- [React-Hook-Form](https://react-hook-form.com/)
- [Framer-Motion](https://www.framer.com/motion/)
- [React-Interestion-Observer](https://github.com/researchgate/react-intersection-observer)

## The Challenge

- [x] View the optimal layout for the app depending on their device's screen size
- [x] See hover states for all interactive elements on the page
- [x] Add/Remove products from the cart
- [x] Edit product quantities in the car
- [x] Fill in all fields in the checkout
- [x] Receive form validations if fields are missed or incorrect during checkout
- [x] See correct checkout totols depending on the products in the cart
  - [x] Shipping always adds $50 to the order
  - [x] VAT is calculated as 20% of the product total, excluding shipping
- [x] See an order confirmation modal after checking out with an order summary
- [x] Bonus: keep track of what's in the cart, even after refreshing the browser(`localStorage` could be used for this if you're not building out a full-stack app)

## How To Use

<!-- Example: -->

```bash
# Clone this repository
$ git clone https://github.com/your-user-name/your-project-name

# Install dependencies
$ npm install # or yarn install

# Run the app
$ npm run dev # or yarn dev
```

## Acknowledgements

> Some videos resources that helped me along the way

- React Motion
  - [Smooth Loading Transitions with Framer Motion | AnimateSharedLayout](https://youtu.be/BtsVMvds3P0)
  - [Let's Learn Framer Motion! (with Matt Perry) — Learn With Jason](https://youtu.be/L_pmBi3m5X0)
- Redux Toolkit
  - [Let’s Learn Modern Redux! (with Mark Erikson) — Learn With Jason](https://youtu.be/9zySeP5vH9c)
  - [React Project: Using the Redux Toolkit](https://youtu.be/GS1VWWCVzG8)
- Chakra UI

  - [Chakra UI Dashboard - With Next.js](https://youtu.be/Ci-pwArI3so)
  - [Chakra UI Quickstart - With NextJS](https://youtu.be/zauiWxYbwPw)

- [Google](https://www.google.com/)

## Contact

- [Twitter](https://{twitter.com/saschamars})
