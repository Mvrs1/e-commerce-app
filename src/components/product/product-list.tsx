import { VStack } from '@chakra-ui/layout'

import CategoryPageItem from '../category-page-item/category-page-item'
import { Product } from '../../models/product'

const ProductList: React.FC<{ products: Product[] }> = ({
  products,
}): JSX.Element => {
  return (
    <VStack
      mt={{ base: '4rem' }}
      spacing={{ base: '7.5rem' }}
      mb={{ base: '7.5rem' }}
    >
      {products.map(product => (
        <CategoryPageItem key={product.id} {...product} />
      ))}
    </VStack>
  )
}

export default ProductList
