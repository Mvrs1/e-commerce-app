import { VStack } from '@chakra-ui/layout'

import YX1Earphones from '../../yx1-earphones/index'
import ZX7Speaker from '../../zx7-speakers/index'
import ZX9Speaker from '../../zx9-speaker/index'

const MainGallery = (): JSX.Element => {
  return (
    <VStack
      mt="7.5rem"
      spacing={{ base: '1.5rem', sm: '2rem', lg: '3rem' }}
      align="stretch"
    >
      <ZX9Speaker />
      <ZX7Speaker />
      <YX1Earphones />
    </VStack>
  )
}

export default MainGallery
