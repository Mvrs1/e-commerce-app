import { Box } from '@chakra-ui/react'

import Navbar from '../../navigation/navbar'
import MobileNav from '../../navigation/mobile-nav'
import SkipLink from '../../navigation/skip-link'

const Header = (): JSX.Element => {
  return (
    <Box
      as="header"
      bg="black"
      py={8}
      borderBottom={['1px', 'unset']}
      borderColor="divider"
    >
      <SkipLink />
      <Navbar />
      <MobileNav />
    </Box>
  )
}

export default Header
