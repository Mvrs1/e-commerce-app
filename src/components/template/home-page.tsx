import { Container } from '@chakra-ui/react'
import Hero from '../ui/hero/hero'
import CategoryLinks from '../ui/category-links/category-links'
import MainGallery from '../ui/main-gallery/main-gallery'
import BestGear from '../best-gear/best-gear'

const HomePage = (): JSX.Element => {
  return (
    <main id="main">
      <Hero />
      <Container maxW="container.lg" px={6}>
        <CategoryLinks />
        <MainGallery />
        <BestGear />
      </Container>
    </main>
  )
}

export default HomePage
