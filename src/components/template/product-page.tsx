import { Product } from '../../models/product'
import { Container } from '@chakra-ui/react'

import ProductDetails from '../product/product-details'
import GoBackLink from '../common/go-back-link'

const ProductPage: React.FC<{ product: Product }> = ({
  product,
}): JSX.Element => {
  return (
    <Container maxW="container.lg" px={6} as="main" id="main">
      <GoBackLink />
      <ProductDetails product={product} />
    </Container>
  )
}

export default ProductPage
