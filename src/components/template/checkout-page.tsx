import { Container } from '@chakra-ui/react'
import GoBackLink from '../common/go-back-link'
import CheckoutForm from '../checkout/checkout-form'

const CheckoutPage = (): JSX.Element => {
  return (
    <Container maxW="container.lg" px={6} as="main" id="main">
      <GoBackLink />
      <CheckoutForm />
    </Container>
  )
}

export default CheckoutPage
