import React from 'react'
import { GetStaticPaths, GetStaticProps } from 'next'
import Head from 'next/head'

import { getProductsPath, getProductBySlug } from '../../utils/products'
import Params from '../../models/params'
import { Product } from '../../models/product'
import ProductPageTemplate from '../../components/template/product-page'

const ProductPage: React.FC<{ product: Product }> = ({
  product,
}): JSX.Element => {
  return (
    <React.Fragment>
      <Head>
        <title>{`Audiophile shop - ${product.name}`}</title>
      </Head>
      <ProductPageTemplate product={product} />
    </React.Fragment>
  )
}

export default ProductPage

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = await getProductsPath().map(path => ({
    params: { category: path.category, slug: path.slug },
  }))

  return {
    paths,
    fallback: false,
  }
}

export const getStaticProps: GetStaticProps = async context => {
  const params = context.params as Params
  const product: Product | undefined = await getProductBySlug(params.slug)

  return {
    props: {
      product,
    },
  }
}
