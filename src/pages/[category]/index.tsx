import Head from 'next/head'
import { useRouter } from 'next/router'
import { GetStaticPaths, GetStaticProps } from 'next'

import CategoryPageTemplate from '../../components/template/category-page'
import { getProductsByCategory, getCategories } from '../../utils/products'
import Params from '../../models/params'
import { Product } from '../../models/product'
import React from 'react'

const CategoryPage = ({ products }: { products: Product[] }): JSX.Element => {
  const {
    query: { category },
  } = useRouter()
  return (
    <React.Fragment>
      <Head>
        <title>{`Audiophile shop - ${category}`}</title>
      </Head>
      <CategoryPageTemplate products={products} />
    </React.Fragment>
  )
}

export default CategoryPage

export const getStaticPaths: GetStaticPaths = async () => {
  const categories: string[] = await getCategories()
  const paths = categories.map(category => ({ params: { category } }))
  return {
    paths,
    fallback: false,
  }
}

export const getStaticProps: GetStaticProps = async context => {
  const params = context.params as Params

  const products: Product[] = await getProductsByCategory(params.category)

  return {
    props: {
      products,
    },
  }
}
