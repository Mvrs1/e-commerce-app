import { ChakraProvider } from '@chakra-ui/react'
import type { AppProps } from 'next/app'
import { Provider } from 'react-redux'
import store from '../store'
import Header from '../components/ui/header/header'
import Footer from '../components/navigation/footer'
import CartModal from '../components/cart/card-modal'
import CheckoutModal from '../components/checkout/checkout-modal'
import Overlay from '../components/common/overlay'
import ModalContextProvider from '../store/modal-context-provider'
import theme from '../styles/theme'

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <ChakraProvider theme={theme} resetCSS>
      <Provider store={store}>
        <ModalContextProvider>
          <Header />
          <Component {...pageProps} />
          <Footer />
          <CartModal />
          <CheckoutModal />
          <Overlay />
        </ModalContextProvider>
      </Provider>
    </ChakraProvider>
  )
}
export default MyApp
