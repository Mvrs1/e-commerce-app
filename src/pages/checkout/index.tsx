import Head from 'next/head'
import CheckoutPageTemplate from '../../components/template/checkout-page'

const CheckoutPage = (): JSX.Element => {
  return (
    <>
      <Head>
        <title>Audiophile shop - checkout</title>
      </Head>
      <CheckoutPageTemplate />
    </>
  )
}

export default CheckoutPage
