import { CartSlice } from '../store/cart-slice'

export const loadCart = (): CartSlice | undefined => {
  try {
    const serialzedCart = localStorage.getItem('cart')
    if (serialzedCart === null) {
      return undefined
    }
    return JSON.parse(serialzedCart)
  } catch (err) {
    return undefined
  }
}

export const saveCart = (cart: CartSlice): void => {
  try {
    const serializedCart = JSON.stringify(cart)
    localStorage.setItem('cart', serializedCart)
  } catch (err) {
    console.log('error while saving cart to local storage', err)
  }
}
