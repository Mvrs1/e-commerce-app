import { configureStore } from '@reduxjs/toolkit'
import { saveCart } from '../utils/local-storage'

import cartReducer from './cart-slice'
import uiReducer from './ui-slice'

const store = configureStore({
  reducer: {
    cart: cartReducer,
    ui: uiReducer,
  },
})

store.subscribe(() => {
  saveCart(store.getState().cart)
})

export type RootState = ReturnType<typeof store.getState>

export default store
